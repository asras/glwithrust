#version 330 core
layout (location = 0) in vec3 pos;
out vec4 color;

void normalize_coords(float x, float y, out float xo, out float yo) {
  // Normalized coordinates that go from 0.0 to 1.0
  xo = (x + 1.0) / 2.0;
  yo = (y + 1.0) / 2.0;
}


const float MINX = -2.0;
const float MAXX = 0.47;
const float MINY = -1.12;
const float MAXY = 1.12;


void map_coords(float x, float y, out float xo, out float yo) {
  // Coordinates that go from minx -> maxx and miny -> maxy
  float xn, yn;
  normalize_coords(x, y, xn, yn);

  xo = xn * (MAXX - MINX) + MINX;
  yo = yn * (MAXY - MINY) + MINY;
}


const int MAX_ITERATIONS = 100;


void main() {
  gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);

  float x0, y0;
  map_coords(pos.x, pos.y, x0, y0);


  float x = 0.0;
  float y = 0.0;

  int iteration = 0;
  while (x * x + y * y <= 2 *2 && iteration < MAX_ITERATIONS) {
    float xtemp = x * x - y * y + x0;
    y = 2 * x * y + y0;
    x = xtemp;
    iteration++;
  }

  if (iteration == MAX_ITERATIONS) {
    iteration = 0;
  }

  float gray = float(iteration) / float(MAX_ITERATIONS);
  color = vec4(gray, gray, gray, 1.0);
}
