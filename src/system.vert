#version 330 core
layout (location = 0) in vec3 pos;
uniform vec2 cthing;
out vec4 color;



void normalize_coords(float x, float y, out float xo, out float yo) {
  // Normalized coordinates that go from 0.0 to 1.0
  xo = (x + 1.0) / 2.0;
  yo = (y + 1.0) / 2.0;
}


float CX = cthing.x;
float CY = cthing.y;
// Choose R > 0 such that R^2 - R > sqrt(CX^2 + CY^2)
const float ESCAPE_RADIUS = 4.0;
const float ASPECT = 600.0 / 800.0;
const float MINX = -1.0;
const float MAXX = 1.0;
const float MINY = -1.0;
const float MAXY =  MINY + (MAXX - MINX) * ASPECT;


void map_coords(float x, float y, out float xo, out float yo) {
  // Coordinates that go from minx -> maxx and miny -> maxy
  float xn, yn;
  normalize_coords(x, y, xn, yn);

  xo = xn * (MAXX - MINX) + MINX;
  yo = yn * (MAXY - MINY) + MINY;
}

const int MAX_ITERATIONS = 200;

void main() {
  gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);

  float zx, zy;
  map_coords(pos.x, pos.y, zx, zy);

  // Custom multiplicative system
  // Three units i, j, k.
  // Multiplication table:
  // ##  i  j  k
  //  i  i  k  j
  //  j  k -i  i
  //  k  j -i -i

  // Function to iterate: f(q) = q * q + c
  // (ax*i + ay*j + az*k) * (ax*i + ay*j + az*k)
  // =   ax^2  * ii   + ax*ay * ij   + ax*az * ik
  //   + ay*ax * ji   + ay^2  * jj   + ay*az * jk
  //   + az*ax * ki   + az*ay * kj   + az^2  * kk
  
  // =   ax^2  *   i  + ax*ay * k    + ax*az * j
  //   + ay*ax *   k  + ay^2  * (-i) + ay*az * i
  //   + az*ax *   j  + az*ay * (-i) + az^2  * (-i)

  // = (ax^2 - ay^2 + ay*az + az^2)*i
  // + (ax*az + az*ax) * j
  // + (ax*ay + ay*ax) * k

  float zz = 0.0;
  int iteration = 0;
  while (zx * zx + zy * zy + zz * zz < ESCAPE_RADIUS * ESCAPE_RADIUS
         && iteration < MAX_ITERATIONS) {

    float nx = zx*zx - zy*zy + zy*zz + zz*zz;
    float ny = zx*zz + zz*zx;
    float nz = zx*zy + zy*zx;

    zx = nx + CX;
    zy = ny + CY;
    zz = nz;

    iteration++;
  }
  
  if (iteration == MAX_ITERATIONS) {
    iteration = 0;
  }

  float gray = pow(float(iteration) / float(MAX_ITERATIONS), 0.5);
  color = vec4(gray, gray, gray, 1.0);
}
