#version 330 core
layout (location = 0) in vec3 pos;
uniform vec2 cthing;
out vec4 color;

// Enable to dynamically adjust scale of image
#define ADJUST_SCALE 0

// There are two color palettes available
#define COLORPALETTE 1

void normalize_coords(float x, float y, out float xo, out float yo) {
  // Normalized coordinates that go from 0.0 to 1.0
  xo = (x + 1.0) / 2.0;
  yo = (y + 1.0) / 2.0;
}

float CX = cthing.x;
float CY = cthing.y;
// Choose R > 0 such that R^2 - R > sqrt(CX^2 + CY^2)
const float ASPECT = 600.0 / 800.0;


#if (ADJUST_SCALE == 1)
float ESCAPE_RADIUS = max(4.0, 2.0 * (CX * CX + CY * CY));
float MINX = -ESCAPE_RADIUS;
float MAXX = ESCAPE_RADIUS;
float MINY = -ESCAPE_RADIUS;
float MAXY =  MINY + (MAXX - MINX) * ASPECT;
#else
const float ESCAPE_RADIUS = 4.0;
const float MINX = -1.0;
const float MAXX = 1.0;
const float MINY = -1.0;
const float MAXY =  MINY + (MAXX - MINX) * ASPECT;
#endif

void map_coords(float x, float y, out float xo, out float yo) {
  // Coordinates that go from minx -> maxx and miny -> maxy
  float xn, yn;
  normalize_coords(x, y, xn, yn);

  xo = xn * (MAXX - MINX) + MINX;
  yo = yn * (MAXY - MINY) + MINY;
}


#if (COLORPALETTE == 1)
const vec3 color1 = 1.2 * vec3(0.1, 0.2, 0.4);
const vec3 color2 = 1.2 * vec3(0.09375, 0.49609375, 0.8984375);
const vec3 color3 = 1.2 * vec3(0.1015625, 0.67578125, 0.88671875);
const vec3 color4 = 1.2 * vec3(0.09375, 0.71484375, 0.69921875);
const vec3 color5 = 1.2 * vec3(0.07421875, 0.66796875, 0.52734375);
#else
const vec3 color1 = vec3(0.4140625, 0.5859375, 0.98828125);
const vec3 color2 = vec3(0.41015625, 0.69140625, 0.98828125);
const vec3 color3 = vec3(1.0, 215.0 / 256.0, 0.0); // GOLD
const vec3 color4 = vec3(0.30078125, 0.9921875, 0.984375);
const vec3 color5 = vec3(1.0, 215.0 / 256.0, 0.5); // GOLD
#endif

const int PALETTE_SIZE = 5;
const vec3 PALETTE[PALETTE_SIZE] = vec3[](color1, color2, color3, color4, color5);

const int MAX_ITERATIONS = 200;

void main() {
  gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);

  float zx, zy;
  map_coords(pos.x, pos.y, zx, zy);

  // Custom multiplicative system
  // Three units i, j, k.
  // Multiplication table:
  // ##  i  j  k
  //  i  i  k  j
  //  j  k -i  i
  //  k  j -i -i

  // Function to iterate: f(q) = q * q + c
  // (ax*i + ay*j + az*k) * (ax*i + ay*j + az*k)
  // =   ax^2  * ii   + ax*ay * ij   + ax*az * ik
  //   + ay*ax * ji   + ay^2  * jj   + ay*az * jk
  //   + az*ax * ki   + az*ay * kj   + az^2  * kk
  
  // =   ax^2  *   i  + ax*ay * k    + ax*az * j
  //   + ay*ax *   k  + ay^2  * (-i) + ay*az * i
  //   + az*ax *   j  + az*ay * (-i) + az^2  * (-i)

  // = (ax^2 - ay^2 + ay*az + az^2)*i
  // + (ax*az + az*ax) * j
  // + (ax*ay + ay*ax) * k

  float zz = 0.0;
  int iteration = 0;
  while (zx * zx + zy * zy + zz * zz < ESCAPE_RADIUS * ESCAPE_RADIUS
         && iteration < MAX_ITERATIONS) {

    float nx = zx*zx - zy*zy + zy*zz + zz*zz;
    float ny = zx*zz + zz*zx;
    float nz = zx*zy + zy*zx;

    zx = nx + CX;
    zy = ny + CY;
    zz = nz;

    iteration++;
  }

  int palette_index = int(round(float(PALETTE_SIZE) * float(iteration) / float(MAX_ITERATIONS)));
  color = vec4(PALETTE[iteration % PALETTE_SIZE], 1.0);
}
