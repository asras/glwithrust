#version 330 core
layout (location = 0) in vec3 pos;
uniform vec2 cthing;
out vec4 color;



void normalize_coords(float x, float y, out float xo, out float yo) {
  // Normalized coordinates that go from 0.0 to 1.0
  xo = (x + 1.0) / 2.0;
  yo = (y + 1.0) / 2.0;
}


/* const float CX = -0.74543; */
/* const float CY = 0.21301; */
float CX = cthing.x;
float CY = cthing.y;
// Choose R > 0 such that R^2 - R > sqrt(CX^2 + CY^2)
const float ESCAPE_RADIUS = 4.0;

/* const float MINX = -ESCAPE_RADIUS; */
/* const float MAXX =  ESCAPE_RADIUS; */
/* const float MINY = -ESCAPE_RADIUS; */
/* const float MAXY =  ESCAPE_RADIUS; */

const float ASPECT = 600.0 / 800.0;
const float MINX = -1.0;
const float MAXX = 1.0;
const float MINY = -1.0;
const float MAXY =  MINY + (MAXX - MINX) * ASPECT;


void map_coords(float x, float y, out float xo, out float yo) {
  // Coordinates that go from minx -> maxx and miny -> maxy
  float xn, yn;
  normalize_coords(x, y, xn, yn);

  xo = xn * (MAXX - MINX) + MINX;
  yo = yn * (MAXY - MINY) + MINY;
}

const vec3 color1 = vec3(0.08203125, 0.34765625, 0.9140625);
const vec3 color2 = vec3(0.09375, 0.49609375, 0.8984375);
const vec3 color3 = vec3(0.1015625, 0.67578125, 0.88671875);
const vec3 color4 = vec3(0.09375, 0.71484375, 0.69921875);
const vec3 color5 = vec3(0.07421875, 0.66796875, 0.52734375);
const int PALETTE_SIZE = 5;
const vec3 PALETTE[PALETTE_SIZE] = vec3[](color1, color2, color3, color4, color5);

const int MAX_ITERATIONS = 200;

void main() {
  gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);

  float zx, zy;
  map_coords(pos.x, pos.y, zx, zy);

  // Custom multiplicative system
  // Three units i, j, k.
  // Multiplication table:
  // ##  i  j  k
  //  i  i  k  j
  //  j  k -i  i
  //  k  j -i -i

  // Function to iterate: f(q) = q * q + c
  // (ax*i + ay*j + az*k) * (ax*i + ay*j + az*k)
  // =   ax^2  * ii   + ax*ay * ij   + ax*az * ik
  //   + ay*ax * ji   + ay^2  * jj   + ay*az * jk
  //   + az*ax * ki   + az*ay * kj   + az^2  * kk
  
  // =   ax^2  *   i  + ax*ay * k    + ax*az * j
  //   + ay*ax *   k  + ay^2  * (-i) + ay*az * i
  //   + az*ax *   j  + az*ay * (-i) + az^2  * (-i)

  // = (ax^2 - ay^2 + ay*az + az^2)*i
  // + (ax*az + az*ax) * j
  // + (ax*ay + ay*ax) * k

  /* float zz = 0.0; */
  /* int iteration = 0; */
  /* while (zx * zx + zy * zy + zz * zz < ESCAPE_RADIUS * ESCAPE_RADIUS */
  /*        && iteration < MAX_ITERATIONS) { */

  /*   float nx = zx*zx - zy*zy + zy*zz + zz*zz; */
  /*   float ny = zx*zz + zz*zx; */
  /*   float nz = zx*zy + zy*zx; */

  /*   zx = nx + CX; */
  /*   zy = ny + CY; */
  /*   zz = nz; */

  /*   iteration++; */
  /* } */
  
  /* if (iteration == MAX_ITERATIONS) { */
  /*   iteration = 0; */
  /* } */

    float x0, y0;
  map_coords(pos.x, pos.y, x0, y0);

  float x = CX;
  float y = CY;

  int iteration = 0;
  while (x * x + y * y <= 2 *2 && iteration < MAX_ITERATIONS) {
    float xtemp = x * x - y * y + x0;
    y = 2 * x * y + y0;
    x = xtemp;
    iteration++;
  }

  if (iteration == MAX_ITERATIONS) {
    iteration = 0;
  }


  int palette_index = int(round(float(PALETTE_SIZE) * float(iteration) / float(MAX_ITERATIONS)));
  color = vec4(PALETTE[iteration % PALETTE_SIZE], 1.0);
  /* float gray = pow(float(iteration) / float(MAX_ITERATIONS), 0.5); */
  /* //float angle = atan(zy, zx); */
  /* //color = vec4(gray, gray, angle / (2.0 * 3.14159265358979323), 1.0); */
  /* color = vec4(gray, gray, gray, 1.0); */
}
