use beryllium::*;
use ogl33::*;

mod shader_tools;
use shader_tools::*;

use core::mem::size_of;

type Vertex = [f32; 3];

const VERT_SHADER: &str = include_str!("shader.vert");
const FRAG_SHADER: &str = include_str!("shader.frag");

fn main() {
    let sdl = SDL::init(InitFlags::Everything).expect("couldn't start SDL");

    sdl.gl_set_attribute(SdlGlAttr::MajorVersion, 3).unwrap();
    sdl.gl_set_attribute(SdlGlAttr::MinorVersion, 3).unwrap();
    sdl.gl_set_attribute(SdlGlAttr::Profile, GlProfile::Core)
        .unwrap();

    let win = sdl
        .create_gl_window(
            "OpenGL in Rust!",
            WindowPosition::Centered,
            800,
            600,
            WindowFlags::Shown,
        )
        .expect("Couldn't make a window and context");

    let mut vertices: Vec<Vertex> = Vec::with_capacity(800 * 600);
    for y in 0..600 {
        for x in 0..800 {
            vertices.push([
                ((x as f32) / 800.0 - 0.5) * 2.0,
                ((y as f32) / 600.0 - 0.5) * 2.0,
                0.0,
            ]);
        }
    }

    win.set_swap_interval(SwapInterval::Vsync);

    unsafe {
        load_gl_with(|f_name| win.get_proc_address(f_name));
    }

    clear_color(0.2, 0.3, 0.3, 1.0);
    let vao = VertexArray::new().expect("Couldn't make a VAO");
    vao.bind();

    let vbo = Buffer::new().expect("Couldn't make a VBO");
    vbo.bind(BufferType::Array);
    buffer_data(
        BufferType::Array,
        bytemuck::cast_slice(&vertices),
        GL_DYNAMIC_DRAW,
    );

    unsafe {
        glVertexAttribPointer(
            0,
            3,
            GL_FLOAT,
            GL_FALSE,
            size_of::<Vertex>().try_into().unwrap(),
            0 as *const _,
        );
        glEnableVertexAttribArray(0);
    }

    let shader_program = ShaderProgram::from_files("system.vert", "shader.frag").unwrap();
    let c_location: i32;
    let name = b"cthing\x00";
    unsafe {
        c_location = glGetUniformLocation(shader_program.0, name.as_ptr().cast());
    }
    if c_location == -1 {
        panic!("Could not find uniform location");
    }    

    shader_program.use_program();

    unsafe {
        glUniform2f(c_location, -0.74543, 0.21301);

    }

    let mut cx = -0.74532;
    let mut cy = 0.21301;
    
    'main_loop: loop {
        while let Some(event) = sdl.poll_events().and_then(Result::ok) {
            match event {
                Event::Quit(_) => break 'main_loop,
                _ => (),
            }
        }

        unsafe {
            glClear(GL_COLOR_BUFFER_BIT);
            glDrawArrays(GL_POINTS, 0, vertices.len().try_into().unwrap());
            cx += 0.00005 * 3.0;
            cy -= 0.00005 * 3.0;
            glUniform2f(c_location, cx, cy);
        }

        win.swap_window();
    }
}
