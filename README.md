# OpenGL in Rust

Based on https://rust-tutorials.github.io/learn-opengl/introduction.html

I've added two utility functions to read shaders from files.

Other than that I've rewritten the main function to render at a per-pixel level and added a few shaders:

1. Mandelbrot set rendering
2. Julia set rendering with a time-varying c-parameter
3. Custom multiplicative system rendering (still z^2 + c but with custom rules for what z * z equals) with time-varying c-parameter
